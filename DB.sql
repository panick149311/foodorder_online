/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2017 (14.0.1000)
    Source Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2017
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/
USE [master]
GO
/****** Object:  Database [FoodOnline_DB]    Script Date: 15/12/2560 18:39:12 ******/
CREATE DATABASE [FoodOnline_DB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'FoodOnline_DB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\FoodOnline_DB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'FoodOnline_DB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\FoodOnline_DB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [FoodOnline_DB] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [FoodOnline_DB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [FoodOnline_DB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [FoodOnline_DB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [FoodOnline_DB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [FoodOnline_DB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [FoodOnline_DB] SET ARITHABORT OFF 
GO
ALTER DATABASE [FoodOnline_DB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [FoodOnline_DB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [FoodOnline_DB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [FoodOnline_DB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [FoodOnline_DB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [FoodOnline_DB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [FoodOnline_DB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [FoodOnline_DB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [FoodOnline_DB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [FoodOnline_DB] SET  ENABLE_BROKER 
GO
ALTER DATABASE [FoodOnline_DB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [FoodOnline_DB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [FoodOnline_DB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [FoodOnline_DB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [FoodOnline_DB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [FoodOnline_DB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [FoodOnline_DB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [FoodOnline_DB] SET RECOVERY FULL 
GO
ALTER DATABASE [FoodOnline_DB] SET  MULTI_USER 
GO
ALTER DATABASE [FoodOnline_DB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [FoodOnline_DB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [FoodOnline_DB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [FoodOnline_DB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [FoodOnline_DB] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'FoodOnline_DB', N'ON'
GO
ALTER DATABASE [FoodOnline_DB] SET QUERY_STORE = OFF
GO
USE [FoodOnline_DB]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [FoodOnline_DB]
GO
/****** Object:  Table [dbo].[CategorySet]    Script Date: 15/12/2560 18:39:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CategorySet](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_CategorySet] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CustomerSet]    Script Date: 15/12/2560 18:39:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerSet](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](max) NOT NULL,
	[Password] [nvarchar](max) NOT NULL,
	[Fname] [nvarchar](max) NOT NULL,
	[Lname] [nvarchar](max) NOT NULL,
	[Address] [nvarchar](max) NOT NULL,
	[Email] [nvarchar](max) NOT NULL,
	[Phone_num] [nvarchar](max) NOT NULL,
	[ConfirmPassword] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_CustomerSet] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FoodSet]    Script Date: 15/12/2560 18:39:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FoodSet](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Price] [nvarchar](max) NOT NULL,
	[Detail] [nvarchar](max) NOT NULL,
	[Image] [nvarchar](max) NOT NULL,
	[CategoryId] [int] NOT NULL,
 CONSTRAINT [PK_FoodSet] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderSet]    Script Date: 15/12/2560 18:39:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderSet](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Status] [nvarchar](max) NOT NULL,
	[FoodId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
 CONSTRAINT [PK_OrderSet] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[CategorySet] ON 

INSERT [dbo].[CategorySet] ([Id], [Name]) VALUES (1, N'Category1')
INSERT [dbo].[CategorySet] ([Id], [Name]) VALUES (2, N'APPETIZER')
INSERT [dbo].[CategorySet] ([Id], [Name]) VALUES (3, N'SOUP')
INSERT [dbo].[CategorySet] ([Id], [Name]) VALUES (4, N'SALADS')
INSERT [dbo].[CategorySet] ([Id], [Name]) VALUES (5, N'U.S. Prime Beef')
INSERT [dbo].[CategorySet] ([Id], [Name]) VALUES (6, N'Australian Beef')
INSERT [dbo].[CategorySet] ([Id], [Name]) VALUES (7, N'SIDE ORDERS')
SET IDENTITY_INSERT [dbo].[CategorySet] OFF
SET IDENTITY_INSERT [dbo].[CustomerSet] ON 

INSERT [dbo].[CustomerSet] ([Id], [Username], [Password], [Fname], [Lname], [Address], [Email], [Phone_num], [ConfirmPassword]) VALUES (1, N'nick30540', N'nick30540', N'Kittisak', N'Thepsit', N'Uniloft ChiangMai', N'nick_am2326@hotmail.com', N'0938275903', N'nick30540')
INSERT [dbo].[CustomerSet] ([Id], [Username], [Password], [Fname], [Lname], [Address], [Email], [Phone_num], [ConfirmPassword]) VALUES (2, N'kuybutane', N'master', N'buwin', N'pro', N'Lamphunn', N'buwin@me.com', N'9999999999', N'master')
SET IDENTITY_INSERT [dbo].[CustomerSet] OFF
SET IDENTITY_INSERT [dbo].[FoodSet] ON 

INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (3, N'FOOD1', N'50', N'Detail1', N'https://img.kapook.com/u/2017/sarinee/July/week3/cok2.jpg', 1)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (4, N'FOOD2', N'50', N'Detail2', N'http://img.tlcdn1.com/travel/2016/02/shutterstock_291024242.jpg', 1)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (5, N'Beef Carpaccio', N'900', N'Truffle emulsion, pickled mushrooms', N'http://img.delicious.com.au/QbrBbpgx/w1200/del/2015/10/classic-beef-carpaccio-12873-1.jpg', 2)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (6, N'4 oz Australian Beef Tartare', N'900', N'Raw Signature Autralian Beef', N'https://i.pinimg.com/736x/c5/7a/36/c57a36fa809b2c04c493e1604b63be72--steak-tartare-polish-food.jpg', 2)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (7, N'Grilled Asparagus, Poached Egg', N'400', N'Shaved Parmesan, truffle dressing', N'https://23991-presscdn-pagely.netdna-ssl.com/wp-content/uploads/grilled-aspargus-prosciutto.jpg', 2)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (8, N'Escargots in Creamy Garlic Butter ', N'490', N'French garlic bread ', N'http://3.bp.blogspot.com/-ItUl_zg3AGE/VLQRJXrW8uI/AAAAAAAAW_o/mZhJqeHHom4/s1600/Escargot%2Bin%2BGarlic%2BButter%2BSauce%2BJanuary%2B18th%2C%2B2014%2B1.jpg', 2)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (9, N'Lump Snow Crab Cakes ', N'530', N'Alaska Crab', N'http://captnchuckystrappe.com/wp-content/uploads/2015/07/crab-cake-asparagus-pic.jpg', 2)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (10, N'Traditional Smoked Salmon', N'590', N'Best Salmon from Japan', N'http://christmas.ebodhitree.com/wp-content/uploads/2011/10/Christmas-smoked-salmon-recipe.jpg', 2)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (11, N'Grilled Hokkaido Scallops ', N'920', N'Citrus reduction, pea emulsion, house smoked bacon lardons', N'https://media-cdn.tripadvisor.com/media/photo-s/08/75/15/79/grilled-hokkaido-scallops.jpg', 2)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (12, N'Lobster Bisque', N'490', N'Alaska Lobster', N'http://bestofsea.com/wp-content/uploads/2015/10/Lobster-Bisque.jpg', 3)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (13, N'Truffle Mushroom Soup', N'450', N'Signature Soup by Our Chef', N'http://www.pepper.ph/wp-content/uploads/2013/10/Knorr-Mushroom-Soup2.jpg', 3)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (14, N'French Onion Soup ', N'380', N'Most Popular Soup', N'https://www.deliaonline.com/sites/default/files/styles/square/public/quick_media/winter-french-onion-soup.jpg?itok=huKeJXbg&c=fa2ba66723f6a96b3b315f2be8a54791', 3)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (15, N'Caesar Salad ', N'530', N'Parmesan cheese, croutons, bacon ', N'http://4.bp.blogspot.com/-lvXI8t4R7Zc/VWSGgmv8ynI/AAAAAAAAWHo/R2yYwEL-YBw/s1600/Roasted%2BPotato%2Band%2BBacon%2BCaesar%2BSalad%2BaIMG_9836fsq.jpg', 4)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (16, N'The Steakhouse Salad ', N'370', N'Herb vinaigrette, roast artichokes, Kalamata olives, shaved Parmesan cheese ', N'https://www.chowstatic.com/assets/2015/03/31348_mexican_grilled_steak_salad_2.jpg', 4)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (17, N'Not Quite a Wedge ', N'390', N'Baby romaine lettuce, warm poached egg, bacon, tomato, scallions, blue cheese croquettes, garlic ranch dressing', N'http://media.cookingandbeer.com/grilledwedgesalad3.jpg', 4)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (18, N'Maine Lobster Salad', N'790', N'Avocado, mango, spring onions, cucumber, celery', N'http://www.lobsterfrommaine.com/wp-content/uploads/2016/03/Tortellini-and-Maine-Lobster-Salad_1140x642_acf_cropped-1.jpg', 4)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (19, N'Crabmeat Salad ', N'660', N'Virgin Mary dressing, confit tomato, avocado purée, sour cream', N'http://cdn-image.myrecipes.com/sites/default/files/image/recipes/sl/03142008/crabmeat-salad-sl-257598-x.jpg', 4)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (20, N'Spinach Salad ', N'390', N'Button mushrooms, cherry tomatoes, spinach leaves, hot bacon dressing ', N'http://food.fnr.sndimg.com/content/dam/images/food/fullset/2012/4/27/0/WU0210H_perfect-spinach-salad_s4x3.jpg.rend.hgtvcom.616.462.suffix/1371606082753.jpeg', 4)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (21, N'Filet Mignon', N'2480', N'8 Oz', N'https://www.kansascitysteaks.com/dyn-images/pdp_hero/Filet-Mignon_cut_FH1-794c0fbb6666471f9882dcb0b7df32d2.jpg', 5)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (22, N'Filet Mignon', N'2880', N'10 Oz', N'https://www.kansascitysteaks.com/dyn-images/pdp_hero/Filet-Mignon_cut_FH1-794c0fbb6666471f9882dcb0b7df32d2.jpg', 5)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (23, N'Rib Eye', N'2950', N'14 Oz', N'https://imagesvc.timeincapp.com/v3/mm/image?url=http%3A%2F%2Fcdn-image.foodandwine.com%2Fsites%2Fdefault%2Ffiles%2Fstyles%2Fmedium_2x%2Fpublic%2F201402-xl-butter-basted-rib-eye-steak.jpg%3Fitok%3D3tgb5z-E&w=700&q=85', 5)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (24, N'NY Striploin', N'2450', N'12 Oz', N'https://cdn.shopify.com/s/files/1/1357/0243/products/Web_ny_striploin_1024x1024.jpg?v=1496145801', 5)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (25, N'Filet Mignon', N'2200', N'8 Oz', N'http://www.atlassteak.com/wp-content/uploads/2016/01/filet-mignon-on-fire.jpg', 6)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (26, N'Filet Mignon', N'2680', N'10 Oz', N'http://www.atlassteak.com/wp-content/uploads/2016/01/filet-mignon-on-fire.jpg', 6)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (27, N'T-Bone', N'2350', N'18 Oz', N'http://thefrenchgrocer.com/shop/18-856-thickbox/australia-t-bone-steak-400gr.jpg', 6)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (28, N'Rib Eye', N'2380', N'14 Oz', N'http://www.entreegrocer.com/wp-content/uploads/2016/05/beef_sirloin_steak-1.jpg', 6)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (29, N'NY Striploin', N'1950', N'12 Oz', N'https://s3-media2.fl.yelpcdn.com/bphoto/b6L7uBdlyGCT2GeCXoIHIg/o.jpg', 6)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (30, N'Porterhouse', N'4350', N'35 Oz', N'https://s3-media2.fl.yelpcdn.com/bphoto/zPnHvWz9TILer1TbAGc2UQ/o.jpg', 6)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (31, N'Baked Potato', N'260', N'With  sour cream, chives, bacon', N'https://www.cravingsofalunatic.com/wp-content/uploads/2017/01/Mini-Potato-Skins-with-Sour-Cream-Bacon-and-Chives-8.jpg', 7)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (32, N'Seasonal Steak Fries', N'150', N'Home Made Fires', N'http://assets.kraftfoods.com/recipe_images/opendeploy/188910_640x428.jpg', 7)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (33, N'Home Made Mashed Potatoes', N'150', N'Signature Mashed Potatoes', N'https://fthmb.tqn.com/ftCNQnbSVDWQLIPcCdHZi6FbQ8A=/960x0/filters:no_upscale()/close-up-of-mashed-potatoes-and-rosemary-554371795-59bbfa7003f4020010e51ebc.jpg', 7)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (34, N'Crisp Thick Onion Rings ', N'130', N'Home Made Onion Rings', N'https://i.pinimg.com/736x/d8/26/d9/d826d9e79abf0915866d5caa6e1877dc--baked-onion-rings-onion-rings-recipe.jpg', 7)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (35, N'Garlic Bread', N'130', N'Home Made Garlic Bread', N'http://www.dinner4tonight.com/img/ultimate-hedgehog-garlic-bread.jpg', 7)
INSERT [dbo].[FoodSet] ([Id], [Name], [Price], [Detail], [Image], [CategoryId]) VALUES (36, N'Baked Cauliflower Cheese', N'250', N'Best Cheese and Bacon', N'http://img.taste.com.au/wyP2-iZc/taste/2016/11/macaroni-and-cauliflower-cheese-bake-78518-1.jpeg', 7)
SET IDENTITY_INSERT [dbo].[FoodSet] OFF
SET IDENTITY_INSERT [dbo].[OrderSet] ON 

INSERT [dbo].[OrderSet] ([Id], [Status], [FoodId], [CustomerId]) VALUES (11, N'True', 3, 1)
INSERT [dbo].[OrderSet] ([Id], [Status], [FoodId], [CustomerId]) VALUES (12, N'True', 12, 2)
INSERT [dbo].[OrderSet] ([Id], [Status], [FoodId], [CustomerId]) VALUES (13, N'True', 4, 2)
INSERT [dbo].[OrderSet] ([Id], [Status], [FoodId], [CustomerId]) VALUES (14, N'True', 8, 2)
INSERT [dbo].[OrderSet] ([Id], [Status], [FoodId], [CustomerId]) VALUES (15, N'True', 15, 2)
INSERT [dbo].[OrderSet] ([Id], [Status], [FoodId], [CustomerId]) VALUES (19, N'True', 30, 2)
INSERT [dbo].[OrderSet] ([Id], [Status], [FoodId], [CustomerId]) VALUES (21, N'False', 4, 2)
INSERT [dbo].[OrderSet] ([Id], [Status], [FoodId], [CustomerId]) VALUES (22, N'True', 9, 1)
INSERT [dbo].[OrderSet] ([Id], [Status], [FoodId], [CustomerId]) VALUES (23, N'False', 5, 1)
INSERT [dbo].[OrderSet] ([Id], [Status], [FoodId], [CustomerId]) VALUES (24, N'False', 15, 1)
SET IDENTITY_INSERT [dbo].[OrderSet] OFF
/****** Object:  Index [IX_FK_CategoryFood]    Script Date: 15/12/2560 18:39:17 ******/
CREATE NONCLUSTERED INDEX [IX_FK_CategoryFood] ON [dbo].[FoodSet]
(
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FK_CustomerOrder]    Script Date: 15/12/2560 18:39:17 ******/
CREATE NONCLUSTERED INDEX [IX_FK_CustomerOrder] ON [dbo].[OrderSet]
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FK_OrderFood]    Script Date: 15/12/2560 18:39:17 ******/
CREATE NONCLUSTERED INDEX [IX_FK_OrderFood] ON [dbo].[OrderSet]
(
	[FoodId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FoodSet]  WITH CHECK ADD  CONSTRAINT [FK_CategoryFood] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[CategorySet] ([Id])
GO
ALTER TABLE [dbo].[FoodSet] CHECK CONSTRAINT [FK_CategoryFood]
GO
ALTER TABLE [dbo].[OrderSet]  WITH CHECK ADD  CONSTRAINT [FK_CustomerOrder] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[CustomerSet] ([Id])
GO
ALTER TABLE [dbo].[OrderSet] CHECK CONSTRAINT [FK_CustomerOrder]
GO
ALTER TABLE [dbo].[OrderSet]  WITH CHECK ADD  CONSTRAINT [FK_OrderFood] FOREIGN KEY([FoodId])
REFERENCES [dbo].[FoodSet] ([Id])
GO
ALTER TABLE [dbo].[OrderSet] CHECK CONSTRAINT [FK_OrderFood]
GO
USE [master]
GO
ALTER DATABASE [FoodOnline_DB] SET  READ_WRITE 
GO
