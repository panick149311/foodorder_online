﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Food_Online.Models
{
    public class UserDetails
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}