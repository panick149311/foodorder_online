﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Food_Online.Models;

namespace Food_Online.Controllers
{
    public class FoodsController : Controller
    {
        private FoodOnline_ModelContainer db = new FoodOnline_ModelContainer();

        // GET: Foods
        public ActionResult Index()
        {
            ViewData["Foods"] = db.FoodSet.ToList();
            ViewData["Categories"] = db.CategorySet.ToList();
            if (User.Identity.Name != "")
            {
                Customer c = db.CustomerSet.FirstOrDefault(u => u.Username == User.Identity.Name);
                ViewData["User"] = c;
            }
            return View();
        }

        // GET: Foods/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Food food = db.FoodSet.Find(id);
            if (food == null)
            {
                return HttpNotFound();
            }
            return View(food);
        }

        // GET: Foods/Create
        public ActionResult Create()
        {
            ViewBag.CategoryId = new SelectList(db.CategorySet, "Id", "Name");
            return View();
        }

        // POST: Foods/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Price,Detail,Image,CategoryId")] Food food)
        {
            if (ModelState.IsValid)
            {
                db.CategorySet.Find(food.CategoryId).Foods.Add(food);
                db.FoodSet.Add(food);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(food);
        }

        // GET: Foods/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Food food = db.FoodSet.Find(id);
            if (food == null)
            {
                return HttpNotFound();
            }
            return View(food);
        }

        // POST: Foods/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Price,Detail,Image,CategoryId")] Food food)
        {
            if (ModelState.IsValid)
            {
                db.Entry(food).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(food);
        }

        // GET: Foods/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Food food = db.FoodSet.Find(id);
            if (food == null)
            {
                return HttpNotFound();
            }
            return View(food);
        }

        // POST: Foods/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Food food = db.FoodSet.Find(id);
            db.CategorySet.Find(food.CategoryId).Foods.Remove(food);
            foreach(var o in db.OrderSet.ToList())
            {
                if (o.FoodId == id)
                {
                    db.OrderSet.Remove(o);
                    db.SaveChanges();
                }
            }
            db.FoodSet.Remove(food);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
