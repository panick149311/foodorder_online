﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Food_Online.Models;

namespace Food_Online.Controllers
{
    public class CustomersController : Controller
    {
        private FoodOnline_ModelContainer db = new FoodOnline_ModelContainer();

        // GET: Customers
        public ActionResult Index()
        {
            return View(db.CustomerSet.ToList());
        }

        // GET: Customers/Details/5
        public ActionResult Details()
        {
            if (User.Identity.Name != "")
            {
                Customer customer = db.CustomerSet.FirstOrDefault(u => u.Username == User.Identity.Name);
                return View(customer);
            }
            else
            {
                return HttpNotFound();
            }
            
        }

        // GET: Customers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Customers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Username,Password,ConfirmPassword,Fname,Lname,Address,Email,Phone_num")] Customer customer)
        {
            if (ModelState.IsValid)
            {
                if (customer.IsUsernameExist())
                {
                    if (customer.IsEmailExist())
                    {
                        db.CustomerSet.Add(customer);
                        db.SaveChanges();
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Email already exist");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Username already exist");
                }
                
            }
            return View(customer);
        }

        // GET: Customers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.CustomerSet.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // POST: Customers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Username,Password,ConfirmPassword,Fname,Lname,Address,Email,Phone_num")] Customer customer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(customer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Details");
            }
            return View(customer);
        }

        // GET: Customers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.CustomerSet.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // POST: Customers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Customer customer = db.CustomerSet.Find(id);
            db.CustomerSet.Remove(customer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
